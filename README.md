# Web terminal

A web terminal with node server.

## Requires
- Node : version 10.13.0 or newer

Install with ```sudo apt install node```

- npm : version 6.14.17 or newer

Install with ```sudo apt install npm```

## Install and run
```
git clone git@gitlab.emi.u-bordeaux.fr:qemunet/webterm.git
cd webterm
npm install

cd webterm/dist
npm install

cd webterm
node app.js
```
(Certificate used is an self-signed certificate created for development testing, you can modify it)

Then, open web browser on http://localhost:3000.

## Options

Usage: ```node app.js [options]```

Options:
```
    -s, --secure         enable secure mode for http
    --sslkey <file>      path to SSL key (default: "./cert/key.pem")
    --sslcert <file>     path to SSL certificate (default: "./cert/server.crt")
    -p, --port <number>  listen port (default: 3000)
    -o, --openid         enable openid authentication
    -h, --help           display help for command
```
