var serverAddress = (location.protocol == "https:" ? "wss://" : "ws://") + location.host;

function connectToSocket(serverAddr) {
    return new Promise(res => {
    const socket = io(serverAddr);
    res(socket);
    });
}

function startTerminal(container, socket) {
    const terminal = new TerminalUI(socket);
    const fitAddon = new FitAddon.FitAddon();
    terminal.terminal.loadAddon(fitAddon);
    terminal.attachTo(container);
    console.log("startTerminal")
    terminal.startListening();
    fitAddon.fit();
    socket.emit('resize', { col: terminal.terminal.cols, row: terminal.terminal.rows } );
    console.log("terminal fitted");
    window.addEventListener('resize', function () {
        fitAddon.fit();
        socket.emit('resize', { col: terminal.terminal.cols, row: terminal.terminal.rows } );
        console.log("terminal fitted");
    });
}

function start() {
    var containers = document.getElementsByClassName("terminal-container");

    if (containers == undefined || containers.length < 1) {
        console.log("Pas de container pour le terminal trouvé");
        return;
    }
    for (let container of containers) {
        console.log("connect on " + serverAddress)
        connectToSocket(serverAddress).then(socket => {
            startTerminal(container, socket);
        });
    }
}

start();